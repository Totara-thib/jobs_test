# Changelog
All notable changes to this job will be documented in this file.

## [0.3.0] - 2022-06-16
* Deploy a static website into the S3 bucket
* Update the docker image from `alpine:3.13` to `alpine:3.16`
* Apply default policy depending on the cloud provider
* Default value for `AWS_ACL`, `AWS_BUCKET_NAME`, `AWS_DEFAULT_REGION` and `AWS_DELETE_OLD_FILE`
* Create a default environnement
* New optional variables:
  * `AWS_PROVIDER`: name of the Cloud Provider (aws or scaleway)
  * `AWS_WEBSITE_HOMEPAGE`: "index.html"
  * `AWS_WEBSITE_ERRORPAGE`: "error.html"
  * `AWS_CLI_VERSION`: "2.7.7"
  * `SNIPPET_POLICY_LINK`: url of the snippet to find policy files

## [0.2.0] - 2021-10-20
* Updating aws CLI to v2
* Adding delete option
* Remove API config variables

## [0.1.0] - 2021-10-10
* Initial version
