## Objective

This job uses the latest AWS CLI version (v2 for now) in order to sync files between a directory and a target S3 bucket. It's compatible with all s3 object storage (not only AWS).

[AWS CLI](https://aws.amazon.com/cli/){:target="_blank"} is a unified tool to manage your AWS services. With just one tool to download and configure, you can control multiple AWS services from the command line and automate them through scripts.

## How to use it
1. Copy the job URL located in the `Install` part of the right panel and add it inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
1. Set your credentials variables in the Gitlab CI/CD variables section of your project (see the [S3 documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html){:target="_blank"} about credentials) .

    ??? summary "Need a custom Endpoint ⚓ ?"
      Just type your custom url in the variable `AWS_ENDPOINT`
      For example, you can find the custom endpoint url for Scaleway [here](https://www.scaleway.com/en/docs/storage/object/api-cli/object-storage-aws-cli/){:target="_blank"}.

   !!! info "Set up a custom bucket policy 👮"
      You can set up a custom bucket policy in the variable `AWS_BUCKET_POLICY`. For example, you can find some documentation for bucket policy for Amazon S3 [here](https://docs.aws.amazon.com/AmazonS3/latest/userguide/access-policy-language-overview.html). This variable should be declared in Gitlab CI/CD variables section as `file`.

1. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](/use-the-hub/#jobs-customization)
1. Well done, your job is ready to work ! 😀

## Job details
* Job name: `aws_s3_sync`
* Docker image: [alpine:3.16](https://hub.docker.com/_/alpine){:target="_blank"}
* Default stage: `deploy`

### Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `AWS_PROVIDER` | Name of the Provider, could be `aws` or `scaleway`  | `aws` |
| `AWS_ACCESS_KEY_ID` | Access key | ` ` |
| `AWS_SECRET_ACCESS_KEY` | Secret key | ` ` |
| `AWS_DEFAULT_REGION` | Region used | `us-east-1` |
| `AWS_SYNC_DIR` | Directory to sync | `build` |
| `AWS_BUCKET_NAME`| The name of the bucket | `bucket-website` |
| `AWS_ACL` | A custom ACL | `public-read` |
| `AWS_ENDPOINT` | Custom endpoint if needed | ` ` |
| `AWS_DELETE_OLD_FILE` | Delete files that exist in the destination but not in the source  | `true` |
| `AWS_BUCKET_POLICY_FILE` | The policy applied to the bucket. If not set will apply `${SNIPPET_POLICY_LINK}/bucket_policy-${AWS_PROVIDER}.json`. Otherwise, it should be declared in Gitlab CI/CD variables section as `file` | ` ` |
| `AWS_DEPLOY_WEBSITE` | Should deploy a static website on the bucket | `true` |
| `AWS_WEBSITE_HOMEPAGE` | The file for the homepage | `index.html` |
| `AWS_WEBSITE_ERRORPAGE` | The file for the error page | `error.html` |
| `AWS_CLI_VERSION` | The version of `AWS` cli | `2.7.7` |
| `SNIPPET_POLICY_LINK` | The link where to fetch policy files | `https://gitlab.com/r2devops/hub/-/snippets/2351961` |
| `IMAGE_TAG` | The default tag for the image | `3.16` |


### Author
This resource is an **[official job](https://docs.r2devops.io/faq-labels/)** added in [**R2Devops repository**](https://gitlab.com/r2devops/hub) by [@GridexX](https://gitlab.com/GridexX)