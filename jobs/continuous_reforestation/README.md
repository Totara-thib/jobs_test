## Objective

Plant trees based on automation with [DigitalHumani](https://docs.digitalhumani.com/) API.


## How to use it

1. Fill the variables `API_KEY` and `ENTERPRISE_ID` inside the GitLab CI/CD variables section of your project. You can retrieve those information inside the [dashboard](https://my.digitalhumani.com/developer) of your DigitalHumani account.
1. Copy the job URL located in the `Install` part of the right panel and add it inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
1. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](/use-the-hub/#jobs-customization)
1. Well done, your job is ready to work ! 😀

### Usage
Plant tree based on GitLab events :

- failed or successful tests.
- a new releases
- a branch merged on main
- a scheduled event (i.e. once per week).
- the carbon footprint of your digital products after deployment.

#### Disclaimer: 
Thought this jobs automates the process of request trees planting. The process itself remains manual and requires people from DigitalHumani to wite an invoice. Due to the amount of work,DigitalHumani accumulates your plant requests until you reach a certain number, depending on your chosen reforestation project, before issuing the order. Below are the least required amounts to receive a monthly invoice and actually plant trees. If you plant more, don't mind this disclaimer.

| Reforestation project | Necessary number of requested trees |
| ------------- | ------------- |
Chase Africa 	| 20
Conserve Natural Forests |	20
OneTreePlanted 	| 1
Sustainable Harvest International 	| 50
TIST 	| 20

## Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `TREE_COUNT` <img width=100/> | Number of trees requested to plant per API call. It costs $1 per tree. <img width=175/>  | `1` <img width=100/> |
| `PROJECT_ID` |  Id of the reforestation project for where the trees will be planted | `81818182` |
| `USER` | End user by whom the trees are planted | `$GITLAB_USER_EMAIL` |
| `ENTERPRISE_ID` | Id of the enterprise. ⚠️ This variable should be specified in `GitLab > CI/CD Settings` | ` ` |
| `API_KEY` | The API key to access the service. ⚠️ This variable should be specified in `GitLab > CI/CD Settings` | ` ` |
| `API_URL` | The url of the API to reach? If you want to performs tests, replace this value by `https://api.sandbox.digitalhumani.com` and update other variables accordingly | `https://api.digitalhumani.com` |
| `IMAGE_TAG` | The default tag for the docker image | `3.16` |

## Author
This resource is an **[official job](https://docs.r2devops.io/faq-labels/)** added in [**R2Devops repository**](https://gitlab.com/r2devops/hub) by [@GridexX](https://gitlab.com/GridexX)
