# Changelog
All notable changes to this job will be documented in this file.

## [0.3.1] - 2021-03-04
* Enable `artifact:expose_as` option to display job result in merge request

## [0.3.0] - 2021-02-16
* Support using space in the documentation title `DOXYGEN_PROJECT_NAME`

## [0.2.0] - 2021-01-29
* Update default output location from `documentation_build/` to `website_build/`

## [0.1.0] - 2020-11-16
* Initial version
