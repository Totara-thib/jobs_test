# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2021-05-28
* Add ability to exit on a particular severity
* Add possibility to append options for command `trivy`
* Change default value for `TRIVY_EXIT_CODE`

## [0.1.0] - 2020-10-21
* Initial version
