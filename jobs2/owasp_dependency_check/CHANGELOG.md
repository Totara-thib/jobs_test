# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2021-04-18
🔄 Switch to use official 🐳 Docker image from OWASP

## [0.1.0] - 2020-11-16
* Initial version