# Changelog
All notable changes to this job will be documented in this file.

## [0.3.0] - 2021-04-23
* Update python version to 3.9

## [0.2.0] - 2020-11-18
* Update python version to 3.8

## [0.1.0] - 2020-10-02
* Initial version
